<#
Loop 30fps

setup:
	draw-field
	set-ships
	send-tcp ready

play:
	ready first starts
	draw-field
	draw-ships
	set-shot
	send-tcp shot
	remote:
		listen-tcp
		compare shot to ships
		draw-hit/miss
		send-tcp hit/miss
	listen-tcp
	if hit/miss draw-hit/miss
	send-tcp pass turn
	remote:
		listen-tcp
		accept pass turn

Field Translation:
	1: 4-6
	2: 8-10
	3: 12-14

	Common math: i*4

	A: 5-6
	B: 8-9
	C: 11-12

	i = Pos in Alphabet
	i = (("I".ToUpper() | Format-Hex).bytes.ToInt64($_) - 64)
	Common Math: i*3+2

Boarder Chars:
	[char]9552-[char]9580

layout:
	ship class

	field example:
		   1  2  3  4  5  6  7  8  9  10
		A [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		B [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		C [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		D [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		E [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		F [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		G [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		H [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		I [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
		J [ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]

		StartScreen:
			arp for relatives
			test port on each relative
			display hostname
			allow selection of hostname
		
		Game Creation:
			user sends {"Id":123412,"NewGame":true}
			prompt remote user to accept sends {"Id":123412,"NewGame":true/false}
			waits for response with same id and answer
		
		Setup:
			both draw game board
			both pick ship pos
			user that sent request starts first

		Turn:
			remote listens for shot
			user enters a coord B7
			local determines if shot is already been taken
			sends {"Id":456345,"Shot":"B7"}
			remote listens for shots
			checks if B7 is hit/miss
			if hit mark ship b7 hit
			sends back {"Id":456345,"Shot":"hit/miss"}
			if hit mark b7 hit
			remote sends {"Id":234524,"Win":true/false}
			if win true, celebrate, ask new game
			if win false and hit true, user enters new cord
			local {"Id":876544,"PassTurn":true}
			local, listens for shot
#>

# Imports
Import-Module -Force .\Assets\Ship.ps1
Import-Module -Force .\Assets\Draw.ps1
Import-Module -Force .\Assets\Remote.ps1
Import-Module -Force .\Assets\StartScreen.ps1

Function Setup-Env {
	$Global:Listener = [Remote]::New(8888)
	$Global:Shots = @()
  $Global:Ships = @()
  $Global:Win = $False
  $Global:Sunk = 0
  $Global:Messages = @()
}

Function Close-Env {
	$Global:Listener.Close()
	$Global:Shots = $Null
	$Global:Ships = $Null
	$Global:Win = $Null
	$Global:Sunk = $Null
	$Global:Messages = $Null
}

Function Validate-Coord($Coord) {
	Try {
		$CY = ($Coord[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)
		$CX = (-Join $Coord[1..2]).ToInt16($_)
		If (($CY -ge 1) -and ($CY -le 10) -and ($CX -ge 1) -and ($CX -le 10)) {
			return $True
		} Else {
			return $False
		}
	}
	Catch {
		return $False
	}
}

Function Action-Turn {
	Draw-Ready Green
	$PassTurn = $False
	While (!($PassTurn)) {
		$Global:Ships.Draw()
		$Host.UI.RawUI.CursorPosition = @{X=16;Y=39}
		"    "
		$Host.UI.RawUI.CursorPosition = @{X=15;Y=39}
  	$Shot = Read-Host "$([char]9553)"
		While (!(Validate-Coord $Shot)) {
			Write-Console "Coordinate: $Shot, is wrong format or out of bounds. Try A1 or J10"
			$Host.UI.RawUI.CursorPosition = @{X=15;Y=39}
			$Shot = Read-Host "$([char]9553)"
		}
		While ($Global:Shots -Contains $Shot) {
			Write-Console "Coordinate: $Shot, has already been fired apon."
			$Host.UI.RawUI.CursorPosition = @{X=15;Y=39}
	  	$Shot = Read-Host "$([char]9553)"
		}
		$Global:Shots += $Shot
    $Props = @{
    	Id = Get-Random -Min 000000 -max 999999
      Shot = $Shot
    }
    $Obj = New-Object -TypeName PSObject -Property $Props
		$Obj = $Obj | ConvertTo-Json
    [Remote]::Send($Obj,$Global:Hostname,$Global:Listener.Port)
    $Answer = $Global:Listener.Listen()
    While (!($Answer.Message.Id -EQ $Props.Id)) {
    	$Answer = $Global:Listener.Listen()
    }
    If ($Answer.Message.Hit) {
		  Draw-Hit $Shot Opponent
			Write-Console "Enemy hit at $Shot!"
			If ($Answer.Message.Sunk) {
				$Global:Sunk++
				Draw-Sunk $Answer.Message.Type "Opponent"
				Write-Console "Enemy $($Answer.Message.Type) sunk!"
			}
			If ($Global:Sunk -EQ 5) {
				$Global:Win = $True
				$Obj = New-Object -TypeName PSObject -Property @{Win = $True}
      	$Obj = $Obj | ConvertTo-Json
      	[Remote]::Send($Obj,$Global:Hostname,$Global:Listener.Port)
      	break
			}
    } Else {
    	Draw-Miss $Shot Opponent
			Write-Console "Missed all enemy ships when firing upon $Shot."
      $Obj = New-Object -TypeName PSObject -Property @{PassTurn = $True}
			$Obj = $Obj | ConvertTo-Json
      [Remote]::Send($Obj,$Global:Hostname,$Global:Listener.Port)
      $PassTurn = $True
			Listen-Turn
    }
  }
}

Function Listen-Turn {
	Draw-Ready Red
	$PassTurn = $False
	While (!($PassTurn)) {
		$Global:Ships.Draw()
		$Question = $Global:Listener.Listen()
		If ($Question.Message -like "*Shot*") {
				[string]$Attempt = $Question.Message.Shot
				ForEach ($Ship in $Global:Ships) {
					If ($Ship.Hit($Attempt)) {
						Write-Console "$($Ship.Type) under fire at $Attempt!"
						If (($Ship.Coords | Where Hit -eq $True).Count -EQ $Ship.Length) {
							$Ship.Sunk = $True
							$ShipTypeAnswer = $Ship.Type
							Draw-Sunk $Ship.Type "Player"
							Write-Console "$($Ship.Type) sunk!"
						}
						$SunkAnswer = $Ship.Sunk
						$HitAnswer = $True
						break
					} Else {
						$SunkAnswer = $Ship.Sunk
						$ShipTypeAnswer = $Null
						$HitAnswer = $False
						Draw-Miss $Attempt Player
					}
				}
				If (!($HitAnswer)) {
					Write-Console "Enemy fire detected but missed at: $Attempt"
				}
				$Props = @{
					Id = $Question.Message.Id
					Hit = $HitAnswer
					Sunk = $SunkAnswer
					Type = $ShipTypeAnswer
				}
				$Obj = New-Object -TypeName PSObject -Property $Props
				$Obj = $Obj | ConvertTo-Json
				[Remote]::Send($Obj,$Global:Hostname,$Global:Listener.Port)
		} ElseIf ($Question.Message -like "*PassTurn*") {
				$PassTurn = $True
				Action-Turn
		} ElseIf ($Question.Message -like "*Win*") {
			$Global:Win = $False
			break
		}
	}
}

Function Start-Game {
	$Global:Ships.Draw()
	If ($Global:First) {
		Action-Turn
	} Else {
		Listen-Turn
	}
	If ($Global:Win) {
		Draw-Win
	} Else {
		Draw-Lose
	}
}

Function Ship-Placement {
	Draw-Board
	Draw-Ready Yellow
	$ShipTypes = @(
		'Carrier',
		'BattleShip',
		'Submarine',
		'Cruiser',
		'Destroyer'
	)
	ForEach ($Type in $ShipTypes) {
		$Ship = [Ship]::New($Type)
		$Ship.Draw()
		If ($Global:Ships.Count -gt 0) {
			$Global:Ships.Draw()
		}
		$Continue = $True
		While($Continue) {
			$Host.UI.RawUI.CursorPosition = @{X=17;Y=39}
			"  "
			$Host.UI.RawUI.CursorPosition = @{X=17;Y=39}
			If ([console]::KeyAvailable) {
				$Key = [System.Console]::ReadKey()
				Switch ($Key.Key) {
					S {
						$_ = $Null
						$ThisCoords = $Ship.Coords.Coord
						$AllCoords = $Global:Ships.Coords.Coord
						$ThisCoords | %{
							If ($AllCoords -Contains $_) {
								$Continue = $True
								Break
							} Else {
								$Continue = $False
							}
						}
					}
					R 						{$_ = $Null;$Ship.Rotate()}
					LeftArrow 		{$_ = $Null;$Ship.Move("Left")}
					RightArrow 		{$_ = $Null;$Ship.Move("Right")}
					UpArrow 			{$_ = $Null;$Ship.Move("Up")}
					DownArrow 		{$_ = $Null;$Ship.Move("Down")}
				}
				Draw-PlayerField
				$Ship.Draw()
				If ($Global:Ships.Count -gt 0) {
      		$Global:Ships.Draw()
    		}	
			} Else {
				Start-Sleep -s 1
			}
		}
		$Global:Ships += $Ship
	}
	$Obj = New-Object -TypeName PSObject -Property @{Ready = $True}
	$Obj = $Obj | ConvertTo-Json -Compress
	[Remote]::Send($Obj,$Global:Hostname,$Global:Listener.Port)
	While ($True) {
		If ($Global:Listener.Listen().Message -like "*Ready*") {
			break
		}
	}
}

Function Main-Loop {
	Try {
		Setup-Env
		StartScreen
		Ship-Placement
		Start-Game
	}
	Finally {
		Close-Env
	}
}

Main-Loop
