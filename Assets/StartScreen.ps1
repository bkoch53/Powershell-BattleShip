	Function FindGames() {
		
	}

	Function DrawGames() {

	}
	
	Function StartScreen() {
		Draw-StartScreen
		$Host.UI.RawUI.CursorPosition = @{X=7;Y=28}
		$Answer = Read-Host "Wait for invitation or Send invitation? (W/s)"
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
		Switch -wildcard ($Answer) {
			"W*" {WaitScreen}
			"S*" {SendScreen}
			Default {WaitScreen}
		}
	}

	Function WaitScreen() {
		Draw-StartScreen
    $Host.UI.RawUI.CursorPosition = @{X=7;Y=28}
    "Waiting for invitation..."
		$Host.UI.RawUI.CursorPosition = @{X=7;Y=28}
		If (ListenNewGame $Global:Listener) {
			$Global:First = $False
		} Else {
			StartScreen
		}
	}

	Function SendScreen() {
		Draw-StartScreen
		$Host.UI.RawUI.CursorPosition = @{X=7;Y=28}
	  $Global:Hostname = Read-Host "Enter a hostname to play against"
	  $Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
		If (AskNewGame $Global:Hostname $Global:Listener) {
			$Global:First = $True
		} Else {
			$Host.UI.RawUI.CursorPosition = @{X=7;Y=28}
			"$Hostname denied the invitation. :("
			Start-Sleep -s 3
			StartScreen
		}
	}

	Function AskNewGame($Hostname) {
		$ObjProp = @{
			Id = Get-Random -Min 000000 -max 999999
			NewGame = $True
			Hostname = $(hostname)
		}
		$Obj = New-Object -TypeName PSObject -Property $ObjProp
		$ObjJson = $Obj | ConvertTo-Json -Compress

		$StartTime = Get-Date
		Try {
			[Remote]::Send($ObjJson,$Hostname,$Global:Listener.Port)
			$Answer = $Global:Listener.Listen()
			While (!($Answer.Message.Id -EQ $Obj.Id) -or (((Get-Date) - $StartTime).TotalSeconds -gt 60)) {
				Start-Sleep -S 2
				$Answer = $Global:Listener.Listen()
			}
			$Answer = $Answer.Message.NewGame
			}
		Catch {
			$Answer = $False
		}
		return $Answer
	}

	Function ListenNewGame() {
		While ($True) {
			$Data = $Global:Listener.Listen()
			If ($Data.Message -like "*NewGame*") {
				$Answer = Read-Host "$($Data.Message.Hostname) requested a new game. Would you like to play? (Y/n)"
				Switch -wildcard ($Answer) {
					"y*" {$Answer = $True}
					"n*" {$Answer = $False}
					default {$Answer = $True}
				}
				$ObjProp = @{
		      Id = $Data.Message.Id
		      NewGame = $Answer
		      Hostname = $(hostname)
		    }
		    $Obj = New-Object -TypeName PSObject -Property $ObjProp
		    $ObjJson = $Obj | ConvertTo-Json -Compress

		    [Remote]::Send($ObjJson,$Data.Message.Hostname,$Global:Listener.Port)
				If ($Answer -eq $True) {
					$Global:Hostname = $Data.Message.Hostname
					return $True
				} Else {
					return $False	
				}
			}
		} 
	}
