Function Draw-Hit([string]$Coord,[string]$Board="Player"){
	ForEach ($Item in $(Translate-Coord $Coord $Board)) {
		$Host.UI.RawUI.CursorPosition = $Item
		Write-Host -f Red "$([char]9608)"
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
	}
}
	
Function Draw-Miss([string]$Coord,[string]$Board="Player"){
	ForEach ($Item in $(Translate-Coord $Coord $Board)) {
		$Host.UI.RawUI.CursorPosition = $Item
		Write-Host -f White "$([char]9608)"
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
	}
}

Function Draw-Ship([string]$Coord,[string]$Board="Player"){
  ForEach ($Item in $(Translate-Coord $Coord $Board)) {
    $Host.UI.RawUI.CursorPosition = $Item
    Write-Host -f DarkGray "$([char]9608)"
    $Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
  }
}

Function Translate-Coord([string]$Coord,[string]$Board="Player") {
	[int]$Y = ($Coord[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)*3+2
	[int]$X = (-Join $Coord[1..2]).ToInt16($_)*4
	If ($Board -like "Player") {
		[int]$X = $X+45
	}
	$Array = @()
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X;Y=[int]$Y}
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X+1;Y=[int]$Y}
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X+2;Y=[int]$Y}
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X;Y=[int]$Y+1}
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X+1;Y=[int]$Y+1}
	$Array += New-Object -TypeName PSObject -Property @{X=[int]$X+2;Y=[int]$Y+1}
	$Array
}

$Global:Field = @"
$([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608) $([char]9608)$([char]9608)$([char]9608)
"@

Function Draw-OpponentField($Color='DarkBlue') {
	$Y = 4
	1..10 | %{
		$Y++
		$Host.UI.RawUI.CursorPosition = @{X=4;Y=$Y}
		Write-Host -f $Color $Global:Field
		$Y++
		$Host.UI.RawUI.CursorPosition = @{X=4;Y=$Y}
    Write-Host -f $Color $Global:Field
		$Y++

		$Host.UI.RawUI.CursorPosition = @{X=0;Y=35}
	}
}

Function Draw-PlayerField($Color='DarkBlue') {
	$Y = 4
	1..10 | %{
    $Y++
    $Host.UI.RawUI.CursorPosition = @{X=49;Y=$Y}
    Write-Host -f $Color $Global:Field
    $Y++
    $Host.UI.RawUI.CursorPosition = @{X=49;Y=$Y}
    Write-Host -f $Color $Global:Field
    $Y++

    $Host.UI.RawUI.CursorPosition = @{X=0;Y=35}
  }
}

Function Draw-Boarder {
	# Top: Top Row
	$Host.UI.RawUI.CursorPosition = @{X=40;Y=0}
	"$([char]9556)$([char]9574)"
	42..49 | %{
    $Host.UI.RawUI.CursorPosition = @{X=$_;Y=0}
    "$([char]9552)"
  }
	$Host.UI.RawUI.CursorPosition = @{X=50;Y=0}
	"$([char]9574)$([char]9559)"

	# Top: Middle Row
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=1}
	"$([char]9556)"
	1..37 | %{
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=1}
		"$([char]9552)"
	}
	$Host.UI.RawUI.CursorPosition = @{X=38;Y=1}
	"$([char]9578)$([char]9578)$([char]9571)BattleShip$([char]9568)$([char]9578)$([char]9578)$([char]9578)"
	54..89 | %{
    $Host.UI.RawUI.CursorPosition = @{X=$_;Y=1}
    "$([char]9552)"
  }
	$Host.UI.RawUI.CursorPosition = @{X=90;Y=1}
  "$([char]9559)"

	$Host.UI.RawUI.CursorPosition = @{X=40;Y=2}
	"$([char]9562)$([char]9577)"
	42..49 | %{
    $Host.UI.RawUI.CursorPosition = @{X=$_;Y=2}
    "$([char]9552)"
  }
	$Host.UI.RawUI.CursorPosition = @{X=50;Y=2}
  "$([char]9577)$([char]9565)"
	
	# Walls
	2..34 | %{
    $Host.UI.RawUI.CursorPosition = @{X=0;Y=$_}
    "$([char]9553)"
  }
	2..34 | %{
    $Host.UI.RawUI.CursorPosition = @{X=90;Y=$_}
    "$([char]9553)"
  }
	
	# Bottom
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=35}
	"$([char]9562)"
	1..89 | %{
    $Host.UI.RawUI.CursorPosition = @{X=$_;Y=35}
    "$([char]9552)"
  }
	$Host.UI.RawUI.CursorPosition = @{X=90;Y=35}
	"$([char]9565)"

	$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
}

Function Draw-PlayerLabels {
	$Host.UI.RawUI.CursorPosition = @{X=14;Y=1}
	"$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=32;Y=1}
  "$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=59;Y=1}
  "$([char]9574)"
  $Host.UI.RawUI.CursorPosition = @{X=78;Y=1}
  "$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=14;Y=2}
	"$([char]9562)$([char]9557)Opponents Fleet$([char]9554)$([char]9565)"
	$Host.UI.RawUI.CursorPosition = @{X=59;Y=2}
  "$([char]9562)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9557)My Fleet$([char]9554)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9565)"
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
}

Function Draw-GridLabels {
	# Draw Letter Coords
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=5};"A"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=8};"B"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=11};"C"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=14};"D"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=17};"E"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=20};"F"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=23};"G"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=26};"H"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=29};"I"
  $Host.UI.RawUI.CursorPosition = @{X=2;Y=32};"J"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=5};"A"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=8};"B"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=11};"C"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=14};"D"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=17};"E"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=20};"F"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=23};"G"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=26};"H"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=29};"I"
  $Host.UI.RawUI.CursorPosition = @{X=47;Y=32};"J"

  # Draw Number Coords
  $Host.UI.RawUI.CursorPosition = @{X=5;Y=4};"1"
  $Host.UI.RawUI.CursorPosition = @{X=9;Y=4};"2"
  $Host.UI.RawUI.CursorPosition = @{X=13;Y=4};"3"
  $Host.UI.RawUI.CursorPosition = @{X=17;Y=4};"4"
  $Host.UI.RawUI.CursorPosition = @{X=21;Y=4};"5"
  $Host.UI.RawUI.CursorPosition = @{X=25;Y=4};"6"
  $Host.UI.RawUI.CursorPosition = @{X=29;Y=4};"7"
  $Host.UI.RawUI.CursorPosition = @{X=33;Y=4};"8"
  $Host.UI.RawUI.CursorPosition = @{X=37;Y=4};"9"
  $Host.UI.RawUI.CursorPosition = @{X=40;Y=4};"10"
  $Host.UI.RawUI.CursorPosition = @{X=50;Y=4};"1"
  $Host.UI.RawUI.CursorPosition = @{X=54;Y=4};"2"
  $Host.UI.RawUI.CursorPosition = @{X=58;Y=4};"3"
  $Host.UI.RawUI.CursorPosition = @{X=62;Y=4};"4"
  $Host.UI.RawUI.CursorPosition = @{X=66;Y=4};"5"
  $Host.UI.RawUI.CursorPosition = @{X=70;Y=4};"6"
  $Host.UI.RawUI.CursorPosition = @{X=74;Y=4};"7"
  $Host.UI.RawUI.CursorPosition = @{X=78;Y=4};"8"
  $Host.UI.RawUI.CursorPosition = @{X=82;Y=4};"9"
  $Host.UI.RawUI.CursorPosition = @{X=86;Y=4};"10"

  $Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
}

Function Draw-Divider {
	# Seperator
	5..33 | %{
    $Host.UI.RawUI.CursorPosition = @{X=45;Y=$_}
    "$([char]9553)"
  }
	$Host.UI.RawUI.CursorPosition = @{X=45;Y=4}
	"$([char]9573)"
	$Host.UI.RawUI.CursorPosition = @{X=45;Y=34}
	"$([char]9576)"
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
}

Function Draw-Board($Color = "DarkBlue") {
	cls
	Draw-PlayerField $Color
	Draw-OpponentField $Color
	Draw-Boarder
	Draw-PlayerLabels
	Draw-GridLabels
	Draw-Divider
	Draw-ShipTracker Opponent
	Draw-ShipTracker Player
	Draw-ConsoleBorder
}	

Function Draw-StartScreen {
	cls
	Draw-Boarder
	$BattleShip = @(
		"	                                    |__",
		"                                     |\/",
		"                                     ---",
		"                                     / | [",
		"                              !      | |||",
		"                            _/|     _/|-++'",
		"                        +  +--|    |--|--|_ |-",
		"                     { /|__|  |/\__|  |--- |||__/",
		"                    +---------------___[}-_===_.'____                 /\",
		"                ____'-' ||___-{]_| _[}-  |     |_[___\==--            \/   _",
		" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7",
		"|                                                                     BB-61/",
		" \_________________________________________________________________________|",
		"~^~^~^^~^~^~^~^~^~^~^~~^~^~^^~~^~~^~^~^^~^~^~^~^~^~^~^~~^~^~^^~~^^~^~^^~~^~^~^"
	)
	$Y = 4
	$BattleShip | %{
		$Y++
		$Host.UI.RawUI.CursorPosition = @{X=7;Y=$Y}
		$_
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
	}
	7..83|%{
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=27}
		"$([char]9552)"
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=29}
    "$([char]9552)"
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
	}
	$Host.UI.RawUI.CursorPosition = @{X=6;Y=27}
	"$([char]9556)"
	$Host.UI.RawUI.CursorPosition = @{X=6;Y=29}
	"$([char]9562)"
	$Host.UI.RawUI.CursorPosition = @{X=84;Y=27}
	"$([char]9559)"
	$Host.UI.RawUI.CursorPosition = @{X=84;Y=29}
	"$([char]9565)"
	$Host.UI.RawUI.CursorPosition = @{X=6;Y=28}
	"$([char]9553)"
	$Host.UI.RawUI.CursorPosition = @{X=84;Y=28}
	"$([char]9553)"

	$Host.UI.RawUI.CursorPosition = @{X=0;Y=38}
}

Function Draw-Ready($Color) {
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=35}
	"$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=15;Y=35}
	"$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=36}
	"$([char]9553)Ready To Fire$([char]9553)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=37}
	"$([char]9568)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9571)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=41}
	"$([char]9562)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9565)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=38}
	"$([char]9553)             $([char]9568)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9559)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=39}
	"$([char]9553)             $([char]9553)     $([char]9553)"
	$Host.UI.RawUI.CursorPosition = @{X=1;Y=40}
	"$([char]9553)             $([char]9568)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9552)$([char]9565)"
	$Host.UI.RawUI.CursorPosition = @{X=2;Y=38}
  Write-Host -f $Color "$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)"
	$Host.UI.RawUI.CursorPosition = @{X=2;Y=39}
  Write-Host -f $Color "$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)"
	$Host.UI.RawUI.CursorPosition = @{X=2;Y=40}
  Write-Host -f $Color "$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)$([char]9608)"
  $Host.UI.RawUI.CursorPosition = @{X=0;Y=42}
}

Function Draw-Win {
	$Text = @(
		'  ___    ___ ________  ___  ___          ___       __   ________  ________   ___',
		' |\  \  /  /|\   __  \|\  \|\  \        |\  \     |\  \|\   __  \|\   ___  \|\  \',
		' \ \  \/  / | \  \|\  \ \  \\\  \       \ \  \    \ \  \ \  \|\  \ \  \\ \  \ \  \',
		'  \ \    / / \ \  \\\  \ \  \\\  \       \ \  \  __\ \  \ \  \\\  \ \  \\ \  \ \  \',
		'   \/  /  /   \ \  \\\  \ \  \\\  \       \ \  \|\__\_\  \ \  \\\  \ \  \\ \  \ \__\',
		' __/  / /      \ \_______\ \_______\       \ \____________\ \_______\ \__\\ \__\|__|',
		'|\___/ /        \|_______|\|_______|        \|____________|\|_______|\|__| \|__|   ___',
		'\|___|/                                                                           |\__\',
		'                                                                                  \|__|'
	)
	$Y = 15
	$Text | %{
		$Host.UI.RawUI.CursorPosition = @{X=5;Y=$Y}
		Write-Host -F Green $_
		$Y++
	}
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=42}
}

Function Draw-Lose {
	$Text = @(
		'  ___    ___ ________  ___  ___          ___       ________  ________  _________  ___',
		' |\  \  /  /|\   __  \|\  \|\  \        |\  \     |\   __  \|\   ____\|\___   ___\\  \',
		' \ \  \/  / | \  \|\  \ \  \\\  \       \ \  \    \ \  \|\  \ \  \___|\|___ \  \_\ \  \',
		'  \ \    / / \ \  \\\  \ \  \\\  \       \ \  \    \ \  \\\  \ \_____  \   \ \  \ \ \  \',
		'   \/  /  /   \ \  \\\  \ \  \\\  \       \ \  \____\ \  \\\  \|____|\  \   \ \  \ \ \__\',
		' __/  / /      \ \_______\ \_______\       \ \_______\ \_______\____\_\  \   \ \__\ \|__|',
		'|\___/ /        \|_______|\|_______|        \|_______|\|_______|\_________\   \|__|     ___',
		'\|___|/                                                        \|_________|            |\__\',
		'                                                                                       \|__|'	
	)
	$Y = 15
  $Text | %{
    $Host.UI.RawUI.CursorPosition = @{X=5;Y=$Y}
    Write-Host -F Red $_
    $Y++
  }
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=42}
}

Function Draw-Sunk($Type,$Board='Player') {
	If ($Board -like "Opponent") {
    $Y = 13
  } Else {
    $Y = 0
  }
	Switch ($Type) {
		'Carrier' {
			$Host.UI.RawUI.CursorPosition = @{X=91;Y=5+$Y}
			Write-Host -F Red -B DarkRed "  Carrier      "
		}
    'BattleShip' {
			$Host.UI.RawUI.CursorPosition = @{X=91;Y=7+$Y}
			Write-Host -F Red -B DarkRed "  BattleShip   "
		}
    'Submarine' {	
			$Host.UI.RawUI.CursorPosition = @{X=91;Y=9+$Y}
			Write-Host -F Red -B DarkRed "  Submarine    "
		}
    'Cruiser' {
			$Host.UI.RawUI.CursorPosition = @{X=91;Y=11+$Y}
			Write-Host -F Red -B DarkRed "  Cruiser      "
		}
    'Destroyer' {
			$Host.UI.RawUI.CursorPosition = @{X=91;Y=13+$Y}
			Write-Host -F Red -B DarkRed "  Destroyer    "
		}
	}
}

Function Draw-ShipTracker($Board='Player') {
	If ($Board -like "Opponent") {
		$Y = 13
		$Text = "Opponents Fleet"
	} Else {
		$Y = 0
		$Text = "    My Fleet"
	}
	$Host.UI.RawUI.CursorPosition = @{X=90;Y=2+$Y}
	"$([char]9568)"
	$Host.UI.RawUI.CursorPosition = @{X=90;Y=4+$Y}
	"$([char]9568)"
	91..105|%{
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=2+$Y}
		"$([char]9552)"
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=4+$Y}
		"$([char]9552)"
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=14+$Y}
		"$([char]9552)"
	}
	$Host.UI.RawUI.CursorPosition = @{X=91;Y=3+$Y}
	"$Text"
	$Host.UI.RawUI.CursorPosition = @{X=90;Y=14+$Y}
	"$([char]9568)"
	
	$Host.UI.RawUI.CursorPosition = @{X=106;Y=2+$Y}
	"$([char]9559)"
	3..13 | %{
		$Host.UI.RawUI.CursorPosition = @{X=106;Y=$_+$Y}
		"$([char]9553)"
	}
	$Host.UI.RawUI.CursorPosition = @{X=106;Y=4+$Y}
	"$([char]9571)"
	$Host.UI.RawUI.CursorPosition = @{X=106;Y=14+$Y}
	"$([char]9565)"

	$Host.UI.RawUI.CursorPosition = @{X=91;Y=5+$Y}
	Write-Host -F Green -B DarkGreen "  Carrier      "
	$Host.UI.RawUI.CursorPosition = @{X=91;Y=7+$Y}
	Write-Host -F Green -B DarkGreen "  BattleShip   "
	$Host.UI.RawUI.CursorPosition = @{X=91;Y=9+$Y}
	Write-Host -F Green -B DarkGreen "  Submarine    "
	$Host.UI.RawUI.CursorPosition = @{X=91;Y=11+$Y}
	Write-Host -F Green -B DarkGreen "  Cruiser      "
	$Host.UI.RawUI.CursorPosition = @{X=91;Y=13+$Y}
	Write-Host -F Green -B DarkGreen "  Destroyer    "

	# Board combining section
	$Host.UI.RawUI.CursorPosition = @{X=98;Y=14}
	"$([char]9574)"
	$Host.UI.RawUI.CursorPosition = @{X=98;Y=15}
	"$([char]9577)"
}

Function Write-Console($Message) {
	$Global:Messages += $Message
	$Y = 38
	$Global:Messages | Select -Last 3 | %{
		$Host.UI.RawUI.CursorPosition = @{X=23;Y=$Y}
		Write-Host "                                                                  "
		$Host.UI.RawUI.CursorPosition = @{X=23;Y=$Y}
  	Write-Host -F Yellow $_
		$Host.UI.RawUI.CursorPosition = @{X=0;Y=42}
  	$Y++
	}
}

Function Draw-ConsoleBorder {
	$Host.UI.RawUI.CursorPosition = @{X=22;Y=35}
  "$([char]9574)"
  $Host.UI.RawUI.CursorPosition = @{X=35;Y=35}
  "$([char]9574)"
	
	$Host.UI.RawUI.CursorPosition = @{X=22;Y=36}
  "$([char]9553)Captains Log$([char]9553)"
  
	$Host.UI.RawUI.CursorPosition = @{X=22;Y=37}
  "$([char]9568)"
	23..88 | %{
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=37}
		"$([char]9552)"
		$Host.UI.RawUI.CursorPosition = @{X=$_;Y=41}
		"$([char]9552)"
	}
  $Host.UI.RawUI.CursorPosition = @{X=35;Y=37}
	"$([char]9577)"
  
	
	# Left Bottom Left Angle
	$Host.UI.RawUI.CursorPosition = @{X=22;Y=41}
  "$([char]9562)"

	# Left Seperator Walls
	$Host.UI.RawUI.CursorPosition = @{X=22;Y=38}
  "$([char]9553)"
  $Host.UI.RawUI.CursorPosition = @{X=22;Y=39}
  "$([char]9553)"
  $Host.UI.RawUI.CursorPosition = @{X=22;Y=40}
  "$([char]9553)"
	
	# Right Top Right Angel
	$Host.UI.RawUI.CursorPosition = @{X=89;Y=37}
	"$([char]9559)"
	# Right Wall
	$Host.UI.RawUI.CursorPosition = @{X=89;Y=38}
	"$([char]9553)"
	$Host.UI.RawUI.CursorPosition = @{X=89;Y=39}
	"$([char]9553)"
	$Host.UI.RawUI.CursorPosition = @{X=89;Y=40}
	"$([char]9553)"
	# Right Bottom Right Angle
  $Host.UI.RawUI.CursorPosition = @{X=89;Y=41}
	"$([char]9565)"
  
	$Host.UI.RawUI.CursorPosition = @{X=0;Y=42}
}
