Class Remote {
  $Listener
  [int]$Port
  Remote($Port){
    $this.Port = $Port
    Try {
      $endpoint = new-object System.Net.IPEndPoint ([system.net.ipaddress]::any, $Port)
      $this.listener = new-object System.Net.Sockets.TcpListener $endpoint
      $this.listener.server.ReceiveTimeout = 3000
      $this.listener.start()
      Write-Host "Listener Started"
    }
    Catch {
      Write-Error "Failed to connect $_"
    }
  }
  Close(){
    $this.Listener.Stop()
    write-host "Listener Stopped."
  }
  [PSObject]Listen(){
    $Client = $Null
    $Obj = New-Object -TypeName PSObject
      Try {
        While (!($this.listener.Pending())) {
          Start-Sleep -Seconds 1
        }
        $Properties = @{
          Sender = $Null
          Message = $Null
        }
        [byte[]]$bytes = 0..255|%{0}
        $client = $this.listener.AcceptTcpClient()
        "$($client.Client.RemoteEndPoint)"
        $Properties.Sender = $($client.Client.RemoteEndPoint)
        If ($Client.Client.Available -ne 0) {
          $stream = $client.GetStream()
          while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0) {
            $Chars = $bytes[0..($i-1)]|%{[char]$_}
            $Properties.Message = -Join $Chars | ConvertFrom-Json
          }
        	$Obj = New-Object -TypeName PSObject -Property $Properties
        } Else {
          # Write-Host "Port Checked"
        }
        $client.Close()
      }
      Catch {
        Write-Error "Failed to handle connection $_"
      }
      Finally {
      }
    return $Obj
  }
  static Send($Data,$Hostname,$Port){
    $TCPClient = $Null
    $IPEndpoint = $Null
    $NetStream = $Null
    Try {
      $ErrorActionPreference = "Stop"
      $TCPClient  = New-Object Net.Sockets.TcpClient
      $IPEndpoint = New-Object Net.IPEndPoint($([Net.Dns]::GetHostEntry($Hostname)).AddressList[0], $Port)
      $TCPClient.Connect($IPEndpoint)
      $NetStream  = $TCPClient.GetStream()
      [Byte[]]$Buffer = [Text.Encoding]::ASCII.GetBytes($Data)
      $NetStream.Write($Buffer, 0, $Buffer.Length)
      $NetStream.Flush()
    }
    Finally {
      If ($NetStream) { $NetStream.Dispose() }
      If ($TCPClient) { $TCPClient.Dispose() }
    }
  }
  [bool]TestAvailable($Hostname) {
    $Hostname = $Null
    $Result = $Null
    $Result = Test-NetConnection -ComputerName $Hostname -Port $this.Port -InformationLevel Quiet
    return $Result
  }
}
