Class Ship {
  [array]$Coords
  [int]$Length
  [bool]$Sunk
  [ValidateSet(
    'Carrier',
    'BattleShip',
    'Submarine',
    'Cruiser',
    'Destroyer')]
  [string]$Type
  Ship(){}
  Ship($Type){
    switch ($Type) {
      'Carrier' {$this.Length = 5}
      'BattleShip' {$this.Length = 4}
      'Submarine' {$this.Length = 3}
      'Cruiser' {$this.Length = 3}
      'Destroyer' {$this.Length = 2}
      Default {Throw}
    }
    $this.Type = $Type
    $this.Sunk = $False
		ForEach ($X in 1..$this.Length) {
			$this.AddCoord("A$X")
		}
  }
  AddCoord($Coord) {
		$this.Coords += New-Object -TypeName PSObject -Property @{
			Coord = "$Coord"
			Hit = $False
		}
    # Checks if both are leagel coords
    # Checks if on correct plane
    # Checks if length is correct for type
    # Adds Coords to $this.Coords
    # Coords should be an object with @{coord=(1,1);hit=bool)
  }
	Move([string]$Direction) {
		$NewMove = $this.NewMove($Direction)
		If ($this.CheckMove($NewMove)) {
			$this.ApplyMove($NewMove)
		} Else {
			# Solid boop
			[console]::beep(350,200)	
		}
	}
	
	[psobject]NewMove([string]$Direction) {
		$NewCoords = @()
		ForEach ($Coord in $this.Coords) {
			$Props = @{
				Coord = $Null
				Hit = $False
			}
			$CY = -($Coord.Coord[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)
      $CX = (-Join $Coord.Coord[1..2]).ToInt16($_)

			 Switch ($Direction) {
        'Left' {
          $_ = $Null
          $CX = $CX - 1
          $CY = $CY * -1
          $CY = $CY + 64
          $CY = $CY.ToChar($_)
          $Props.Coord = "$CY$CX"
        }
        'Right' {
          $_ = $Null
          $CX = $CX + 1
          $CY = $CY * -1
          $CY = $CY + 64
          $CY = $CY.ToChar($_)
          $Props.Coord = "$CY$CX"
        }
        'Up' {
          $_ = $Null
          $CY = $CY + 1
          $CY = $CY * -1
          $CY = $CY + 64
          $CY = $CY.ToChar($_)
          $Props.Coord = "$CY$CX"
        }
        'Down' {
          $_ = $Null
          $CY = $CY - 1
          $CY = $CY * -1
          $CY = $CY + 64
          $CY = $CY.ToChar($_)
          $Props.Coord = "$CY$CX"
        }
      }
			$NewCoords += New-Object -TypeName PSObject -Property $Props
		}
		return $NewCoords
	}
	
	[bool]CheckMove($Coords) {
		$CanMove = $False
		ForEach ($Coord in $Coords) {
			$CY = ($Coord.Coord[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)
      $CX = (-Join $Coord.Coord[1..2]).ToInt16($_)
			If (($CY -ge 1) -and ($CY -le 10) -and ($CX -ge 1) -and ($CX -le 10)) {
				$CanMove = $True
			} Else {
				return $False
			}
		}
		return $CanMove
	}
	
	ApplyMove($Coords) {
		$this.Coords = $Coords
	}


	Rotate() {
		$Origin = $this.Coords.Coord[0]
		$Y = -($Origin[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)
		$X = (-Join $Origin[1..2]).ToInt16($_)
		
		$NewCoords = @()
    ForEach ($Coord in $this.Coords) {
      $Props = @{
        Coord = $Null
        Hit = $False
      }
			$CY = -($Coord.Coord[0].ToString().ToUpper().ToChar($_).ToByte($_)-64)
			$CX = (-Join $Coord.Coord[1..2]).ToInt16($_)
			$CX = $CX - $X
			$CY = $CY - $Y
			$CX, $CY = $CY, $(-$CX)
			$CX = $CX + $X
			$CY = $CY + $Y
			$CY = (($CY* -1)+64).ToChar($_)
			$Props.Coord = "$CY$CX"

			$NewCoords += New-Object -TypeName PSObject -Property $Props
		}
		If ($this.CheckMove($NewCoords)) {
      $this.ApplyMove($NewCoords)
    } Else {
      # Solid boop
      [console]::beep(350,200)
    }
	}
	[bool]Hit([string]$Shot) {
		$Hit = $False
		ForEach ($Coord in $this.Coords) {
			If ($Coord.Coord -Contains $Shot) {
				$Coord.Hit = $True
				$Hit = $True
			}
		}
		return $Hit 
	}
  Draw() {
    ForEach ($Coord in $this.Coords) {
			If ($Coord.Hit) {
	     	Draw-Hit $Coord.Coord Player
			} Else {
	     	Draw-Ship $Coord.Coord Player
			}
    }
  }
}
